<?php

/**
 * class SpectaclesController
 * @package App\Controller
 * @Route("/spectacles", name="spectacles")
 */
namespace App\Controller;

use App\Repository\SpectacleRepository;
use ContainerSx5xKry\getSpectacleRepositoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Spectacle;

class SpectaclesController extends AbstractController
{
    #[Route('/spectacles', name: 'app_spectacles')]
    public function spectacles(SpectacleRepository $spectRepo): Response
    {
        $spectacles = $spectRepo->findAll();
        return $this->render('default/spectacle.html.twig', [
            'Spectacles' => $spectacles,
        ]);
    }
}
