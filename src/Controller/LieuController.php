<?php

namespace App\Controller;

use App\Repository\LieuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LieuController extends AbstractController
{
    #[Route('/lieux', name: 'app_lieu')]
    public function Lieu(LieuRepository $lieuRepo): Response
    {
        $lieu = $lieuRepo->findAll();
        return $this->render('default/Lieu.html.twig', [
            'Lieu' => $lieu,
        ]);
    }
}
