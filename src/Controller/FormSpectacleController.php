<?php

namespace App\Controller;

use App\Entity\Spectacle;
use App\Form\AddSpectacleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use Symfony\Component\Routing\Annotation\Route;

class FormSpectacleController extends AbstractController
{
    #[Route('/form/spectacleAdd', name: 'app_form_spectacle')]
    public function AddSpectacleForm(Request $request, EntityManagerInterface $em): Response
    {
        $spectacle = new Spectacle('',"","",new DateTime('now'),null);
        $form = $this->createForm(AddSpectacleType::class, $spectacle);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$NameSpectacle = $spectacle->getNom();
            $formData = $form->getData();
            $em->persist($formData);
            $em->flush();

            return $this->redirectToRoute('app_form_spectacle', [
                'message' => 'le message a bien été envoyé'
            ]); // Assurez-vous d'ajuster le nom de la route
        }

        return $this->render('form_spectacle/SpectacleFormAdd.html.twig', [
            'FormAddSpectacle' => $form,
        ]);
    }
}
