<?php

namespace App\Controller;

use App\Entity\Spectacle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
        ]);
    }
    #[Route('/spectacle/test', name: 'app_spectacle')]
    public function testSpectacle(): Response
    {
        $spec1 = new Spectacle("Cabaret","Venez assistez au plus grand cabaret du monde","images/1.jpg", new \DateTime('now'),null);
        return $this->render('default/spectacleTest.html.twig', [
            'spectacleTest' => $spec1,
        ]);
    }


}
