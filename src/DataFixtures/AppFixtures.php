<?php

namespace App\DataFixtures;

use App\Entity\Lieu;
use App\Entity\Spectacle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        $tabLieux = array();
        for ($i = 0; $i < 20; $i++) {
            $lieu = new Lieu("", "", "", "");
            $lieu->setNom('Lieu' . $i);
            $lieu->setAdresse('Adresse' . $i);
            $lieu->setCodePostal('' . rand(1000, 95000));
            $lieu->setVille('Ville' . $i);
            $tabLieux[] = $lieu;
            $this->addReference('lieu' . $i, $lieu);
            $manager->persist($lieu);
        }

        for ($i = 0; $i < 20; $i++) {
            $spectacles = new Spectacle("", "", "", new \DateTime('now'), null);
            $spectacles->setNom('Spectacles' . $i);
            $spectacles->setDescription('spectacles' . $i);
            $spectacles->setImage('images/' . $i . '.jpg');
            $spectacles->setDate(new \DateTime('now'));
            $randomLieu = $this->getReference('lieu' . array_rand($tabLieux));
            $spectacles->setLieu($randomLieu);
            $manager->persist($spectacles);
        }
        // $manager->persist($product);
        $manager->flush();

    }

}


